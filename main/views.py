from django.shortcuts import render, redirect
from rest_framework.response import Response
import requests
import json

# Create your views here.

def homepage(request):
    response = requests.get('https://django-inventory-project.herokuapp.com/get_product/', params=request.GET)
    data = response.json()
    return render(request, 'home.html', {'context': data})
